package com.tdewandeleer.loveconnector;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Similarity {
	private Graph graphA;
	private Graph graphB;
	private List<List<Integer>> inNodeListA;
	private List<List<Integer>> outNodeListA;
	private List<List<Integer>> inNodeListB;
	private List<List<Integer>> outNodeListB;
	private Double[][] nodeSimilarity;
	private Double[][] inNodeSimilarity;
	private Double[][] outNodeSimilarity;
	private Double epsilon;
	private int graphSizeA;
	private int graphSizeB;

	public Similarity(Graph graphA, Graph graphB, Double epsilon) {
		try {
			this.graphA = graphA;
			this.graphB = graphB;
			this.epsilon = epsilon;
			this.inNodeListA = graphA.getInDegreeNodeList();
			this.outNodeListA = graphA.getOutDegreeNodeList();
			this.inNodeListB = graphB.getInDegreeNodeList();
			this.outNodeListB = graphB.getOutDegreeNodeList();

			this.graphSizeA = graphA.getGraphSize();
			this.graphSizeB = graphB.getGraphSize();

			this.nodeSimilarity = new Double[graphSizeA][graphSizeB];
			this.inNodeSimilarity = new Double[graphSizeA][graphSizeB];
			this.outNodeSimilarity = new Double[graphSizeA][graphSizeB];

			initializeMatrices();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initializeMatrices() {
		for (int i = 0; i < graphSizeA; i++) {
			for (int j = 0; j < graphSizeB; j++) {
				Double maxDegree = Double.valueOf(Math.max(inNodeListA.get(i).size(), inNodeListB.get(j).size()));
				if (maxDegree != 0) {
					inNodeSimilarity[i][j] = ((Math.min(inNodeListA.get(i).size(), inNodeListB.get(j).size())) / (maxDegree));
				} else {
					inNodeSimilarity[i][j] = Double.valueOf(0);
				}

				maxDegree = Double.valueOf(Math.max(outNodeListA.get(i).size(), outNodeListB.get(j).size()));
				if (maxDegree != 0) {
					outNodeSimilarity[i][j] = ((Math.min(outNodeListA.get(i).size(), outNodeListB.get(j).size())) / (maxDegree));
				} else {
					outNodeSimilarity[i][j] = Double.valueOf(0);

				}
			}
		}

		for (int i = 0; i < graphSizeA; i++) {
			for (int j = 0; j < graphSizeB; j++) {
				nodeSimilarity[i][j] = (inNodeSimilarity[i][j] + outNodeSimilarity[i][j]) / 2;
			}
		}
	}

	public void iterationPhase() {
		double maxDifference = 0.0;
		boolean terminate = false;

		while (!terminate) {
			maxDifference = 0.0;
			for (int i = 0; i < graphSizeA; i++) {
				for (int j = 0; j < graphSizeB; j++) {
					if (!(graphA.getNodeType(i).equals(graphB.getNodeType(j)))){
						inNodeSimilarity[i][j] = 0.0;
					}
					else{
						double similaritySum = 0.0;
						double maxDegree = Double.valueOf(Math.max(inNodeListA.get(i).size(), inNodeListB.get(j).size()));
						double minDegree = Double.valueOf(Math.min(inNodeListA.get(i).size(), inNodeListB.get(j).size()));

						if (minDegree == inNodeListA.get(i).size()) {
							similaritySum = optimizationFunction(inNodeListA.get(i), inNodeListB.get(j), 0);
						} else {
							similaritySum = optimizationFunction(inNodeListB.get(j), inNodeListA.get(i), 1);
						}

						if (maxDegree == 0.0 && similaritySum == 0.0) {
							inNodeSimilarity[i][j] = 1.0;
						} else if (maxDegree == 0.0) {
							inNodeSimilarity[i][j] = 0.0;
						} else {
							inNodeSimilarity[i][j] = similaritySum / maxDegree;
						}
					}

					if (!(graphA.getNodeType(i).equals(graphB.getNodeType(j)))){
						outNodeSimilarity[i][j] = 0.0;
					}
					else {
						double similaritySum = 0.0;
						double maxDegree = Double.valueOf(Math.max(outNodeListA.get(i).size(), outNodeListB.get(j).size()));
						double minDegree = Double.valueOf(Math.min(outNodeListA.get(i).size(), outNodeListB.get(j).size()));

						if (minDegree == outNodeListA.get(i).size()) {
							similaritySum = optimizationFunction(outNodeListA.get(i), outNodeListB.get(j), 0);
						} else {
							similaritySum = optimizationFunction(outNodeListB.get(j), outNodeListA.get(i), 1);
						}


						if (maxDegree == 0.0 && similaritySum == 0.0) {
							outNodeSimilarity[i][j] = 1.0;
						} else if (maxDegree == 0.0) {
							outNodeSimilarity[i][j] = 0.0;
						} else {
							outNodeSimilarity[i][j] = similaritySum / maxDegree;
						}
					}
				}
			}

			for (int i = 0; i < graphSizeA; i++) {
				for (int j = 0; j < graphSizeB; j++) {
					double temp = (inNodeSimilarity[i][j] + outNodeSimilarity[i][j]) / 2;
					if (Math.abs(nodeSimilarity[i][j] - temp) > maxDifference) {
						maxDifference = Math.abs(nodeSimilarity[i][j] - temp);
					}
					nodeSimilarity[i][j] = temp;
				}
			}

			if (maxDifference < epsilon) {
				terminate = true;
			}
		}
		DecimalFormat f = new DecimalFormat("#.##");

		for (int i = 0; i < graphSizeA; i++) {
			for (int j = 0; j < graphSizeB; j++) {
				nodeSimilarity[i][j] = (Double.valueOf((f.format(nodeSimilarity[i][j])).replace(",",".")));
			}
		}
	}


	public Double getGraphSimilarity() {
		Double finalGraphSimilarity = 0.0;
		DecimalFormat f = new DecimalFormat("0.000");
		iterationPhase();

		if (graphA.getGraphSize() < graphB.getGraphSize()) {
			finalGraphSimilarity = optimizationFunction(graphA.getNodeList(), graphB.getNodeList(), 0) / graphA.getGraphSize();
		} else {
			finalGraphSimilarity = optimizationFunction(graphB.getNodeList(), graphA.getNodeList(), 1) / graphB.getGraphSize();
		}
		finalGraphSimilarity = Double.valueOf((f.format(finalGraphSimilarity*100)).replace(",","."));		
		return finalGraphSimilarity;
	}

	public Double getNodeSimilarity(String typeA, String valueA, String typeB, String valueB){
		if(graphA.nodeExists(typeA, valueA) && graphB.nodeExists(typeB, valueB)){
			int indexA = graphA.getNodeIndex(typeA, valueA);
			int indexB = graphB.getNodeIndex(typeB, valueB);
			return nodeSimilarity[indexA][indexB]*100;
		}
		else{
			System.err.println("Node does not exist");
			return -1.0;
		}
	}

	public void printMatrix(Double[][] matrix){
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(" "+matrix[i][j]+" ");
			}
			System.out.println();
		}
	}

	public void printMatrix(double[][] matrix){
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(" "+matrix[i][j]+" ");
			}
			System.out.println();
		}
	}

	public double optimizationFunction(List<Integer> neighborListMin, List<Integer> neighborListMax, int graph) {
		double similaritySum = 0.0;
		Map<Integer, Double> valueMap = new HashMap<Integer, Double>();
		if (graph == 0) {
			if (neighborListMin.size()>0) {
				double[][] cost=new double[neighborListMin.size()][neighborListMax.size()];
				for (int i = 0; i < neighborListMin.size(); i++) {
					int node = neighborListMin.get(i);
					for (int j = 0; j < neighborListMax.size(); j++) {
						int key = neighborListMax.get(j);
						cost[i][j]=0-nodeSimilarity[node][key];
					}
				}
				HungarianBipartiteMatching hbm = new HungarianBipartiteMatching(cost);
				int[] result = hbm.execute();

				for(int i=0;i<result.length;i++){
					valueMap.put(i,(cost[i][result[i]]*-1));
				}

			}
		} else {	
			if (neighborListMin.size()>0) {
				double[][] cost=new double[neighborListMin.size()][neighborListMax.size()];
				for (int i = 0; i < neighborListMin.size(); i++) {
					int node = neighborListMin.get(i);
					for (int j = 0; j < neighborListMax.size(); j++) {
						int key = neighborListMax.get(j);
						cost[i][j]=0-nodeSimilarity[key][node];
					}
				}
				HungarianBipartiteMatching hbm = new HungarianBipartiteMatching(cost);
				int[] result = hbm.execute();

				for(int i=0;i<result.length;i++){
					valueMap.put(i,(cost[i][result[i]]*-1));
				}

			}
		}

		for (double value : valueMap.values()) {
			similaritySum += value;
		}
		return similaritySum;
	}

}
