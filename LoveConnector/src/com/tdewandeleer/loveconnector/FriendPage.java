package com.tdewandeleer.loveconnector;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendPage extends Activity implements android.view.View.OnClickListener{

	public String name;
	public String ID;
	public String myId;
	public String me;
	public String birthdayString;
	public String statusString;
	public String genderString;
	ArrayList<String> myArray = new ArrayList<String>();
	ImageButton compare;
	ListView listViewFriend;
	TextView nameView;
	TextView birthday;
	TextView status;
	ImageView profilePicture;
	ProgressDialog progressDialog; 

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		setContentView(R.layout.friendpage);

		Intent intent = getIntent();
		name = intent.getStringExtra("name");
		ID = intent.getStringExtra("id");
		myId = intent.getStringExtra("myId");
		me = intent.getStringExtra("me");

		new LoadViewTask().execute();

	}

	//To use the AsyncTask, it must be subclassed
	private class LoadViewTask extends AsyncTask<Void, Integer, Void>
	{
		//Before running code in separate thread
		@Override
		protected void onPreExecute()
		{
			progressDialog = new ProgressDialog(FriendPage.this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setTitle("Loading...");
			progressDialog.setMessage("Loading profile information, please wait...");
			progressDialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(false);
			progressDialog.show();
		}

		//The code to be executed in a background thread.
		@Override
		protected Void doInBackground(Void... params)
		{
			//Get the current thread's token
			synchronized (this)
			{
				sendRequest(ID);
			}
			return null;
		}
		//after executing the code in the thread
		@Override
		protected void onPostExecute(Void result)
		{
			//close the progress dialog
			progressDialog.dismiss();

			initView();
		}
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case R.id.imageButton1:
			Intent intent=new Intent(this, Comparison.class);
			intent.putExtra("name", name);
			intent.putExtra("id", ID);
			intent.putExtra("myId", myId);
			startActivity(intent);
			break;
		}
	}

	public void initView(){
		compare=(ImageButton) findViewById(R.id.imageButton1);
		compare.setOnClickListener(FriendPage.this);

		listViewFriend=(ListView) findViewById(R.id.listViewFriend);

		nameView=(TextView) findViewById(R.id.textFriend);
		nameView.setText(name);
		birthday=(TextView) findViewById(R.id.textView2);
		birthday.setText(birthdayString);
		status=(TextView) findViewById(R.id.textView3);
		status.setText(statusString);

		if (me.equals("Y")) {
			compare.setVisibility(View.GONE);
		}
		else{
			compare.setEnabled(true);
		}

		profilePicture = (ImageView) findViewById(R.id.profilepicture);

		ArrayAdapter<String> adapt=new ArrayAdapter<String>(getApplicationContext(), R.layout.mytextview, myArray);
		listViewFriend.setAdapter(adapt);

		getProfilePicture(ID, profilePicture);
	}

	public void sendRequest(String id){

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), id);

		Response response = getCat.executeAndWait();

		List<String[]> parsedCharacteristics=parseMe(response);

		for(String[] x:parsedCharacteristics){
			if (x.length==2) {
				String temp = x[0].substring(0, 1).toUpperCase() + x[0].substring(1); 
				myArray.add(temp+" : "+x[1]);
				if(x[0].equals("gender")){
					genderString=x[1];
				}
				if(x[0].equals("relationship_status")){
					statusString=x[1];
				}
				if(x[0].equals("birthday")){
					birthdayString=x[1];
				}

			}
			else if (x.length==3) {
				String temp = x[0].substring(0, 1).toUpperCase() + x[0].substring(1); 
				myArray.add(temp+" : "+x[1]+ " ("+x[2]+")");
			}
			else{
				System.out.println("???");
				System.out.println(x.toString());
			}
		}

	}

	public void getProfilePicture(String id,ImageView profilePicture){
		try {
			URL image_value = new URL("https://graph.facebook.com/"+id+ "/picture?type=large");
			Bitmap bmp = null;
			try {
				bmp = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
			profilePicture.setImageBitmap(roundCornerImage(bmp, 100));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public List<String[]> parseMe(Response user){
		String[] soloCharacteristics={"bio","birthday","locale","link","first_name","timezone","email","verified","name","last_name","gender","id","relationship_status"}; //13
		String[] multiCharacteristics={"inspirational_people","location","sports","languages","favorite_athletes","hometown","education"}; //8
		String[] tableCharacteristics={"interested_in"}; //1
		ArrayList<String[]> informations=new ArrayList<String[]>();
		JSONObject list=user.getGraphObject().getInnerJSONObject();
		for(String key:soloCharacteristics){
			try {
				String value=list.getString(key);
				String[] toAdd={key,value};
				informations.add(toAdd);
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:multiCharacteristics){
			try {
				String valuelist=list.getString(key);
				String result=parseResponse(valuelist);
				String[] resultList=result.split(",");
				for(String x:resultList){
					String[] valueAndCategory=getCategory(x);
					String[] toAdd={key,valueAndCategory[0],valueAndCategory[1]};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:tableCharacteristics){
			try {
				String value=list.getString(key);
				value=value.replace("[", "").replace("]", "").replace("\"", "");
				String[] valueList=value.split(",");
				for(String x:valueList){
					String[] toAdd={key,x};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		return informations;
	}

	public static String parseResponse(String test){
		String result="";
		String[] open=test.split("(\\{)|(,)|(\\})");
		boolean with=false;
		int fromCount=0;
		for(String x:open){
			if(!x.equals("")){
				if(fromCount==0){
					if(x.contains("from")){
						fromCount=2;
					}
					else{
						if(x.contains("]")){
							with=false;
						}
						else if(x.contains("with") || with==true){
							with=true;
						}
						else{
							if(x.contains("\"id\"")){
								String end=x.replace("\"", "").replace("name:", "");
								if(result.equals("")){
									result+=end;
								}
								else{
									result+=","+end;
								}
							}
						}
					}
				}
				else{
					fromCount--;
				}

			}
		}
		result=result.replace("id", "").replace(":", "");
		return result;
	}

	public String[] getCategory(String id){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), id);
		Response response = getCat.executeAndWait();
		String[] result={response.getGraphObject().getProperty("name").toString(),response.getGraphObject().getProperty("category").toString()};
		return result;
	}

	public Bitmap roundCornerImage(Bitmap src, float round) {
		// Source image size
		int width = src.getWidth();
		int height = src.getHeight();
		// create result bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// configure paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// configure rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw Round rectangle to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}
}
