package com.tdewandeleer.loveconnector;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;


public class Comparison extends Activity implements android.view.View.OnClickListener {

	TextView structure;
	TextView content;
	TextView score;
	TextView press;
	ImageButton theButton;
	String friendsId;
	String myId;
	String name;
	Graph myGraph;
	Graph friendGraph;
	String userString="User";
	String categoryString="Category";
	double structureSim;
	double contentSim;
	ImageView myPicture;
	ImageView friendPicture;
	ImageView phrase;
	ImageView yourscore;
	ImageView star;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comparison);

		score=(TextView) findViewById(R.id.textView2);
		structure=(TextView) findViewById(R.id.textView4);
		content=(TextView) findViewById(R.id.textView5);
		press=(TextView) findViewById(R.id.textView6);

		theButton=(ImageButton) findViewById(R.id.button1);
		theButton.setOnClickListener(this);

		myPicture=(ImageView) findViewById(R.id.imageView1);
		friendPicture=(ImageView) findViewById(R.id.imageView2);
		phrase=(ImageView) findViewById(R.id.imageView4);
		yourscore=(ImageView) findViewById(R.id.imageView3);
		star=(ImageView) findViewById(R.id.imageView5);

		Intent intent = getIntent();
		name=intent.getStringExtra("name");
		friendsId=intent.getStringExtra("id");
		myId=intent.getStringExtra("myId");

		myGraph = new Graph();
		friendGraph = new Graph();

		showInformation();
	}

	public void showInformation(){
		getProfilePicture(myId, myPicture);
		getProfilePicture(friendsId, friendPicture);
	}

	public void getProfilePicture(String id,ImageView profilePicture){
		if(id!=null){
			try {
				URL image_value = new URL("https://graph.facebook.com/"+id+ "/picture?type=large");
				Bitmap bmp = null;
				try {
					bmp = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				BitmapDrawable ob = new BitmapDrawable(getResources(), roundCornerImage(bmp, 100));
				profilePicture.setBackgroundDrawable(ob);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		else{
			profilePicture.setBackgroundResource(R.drawable.male);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button1:
			final ProgressDialog progress;
			progress = ProgressDialog.show(this, "Computing Score","Your score is being computed", true);
			progress.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;
			new Thread(new Runnable() {
				@Override
				public void run()
				{
					theMethod();
					runOnUiThread(new Runnable() {
						@Override
						public void run()
						{
							postMethod();
							progress.dismiss();
						}
					});
				}
			}).start();
			break;
		}
	}

	public void theMethod(){

		List<String []> myinfo=fillMyInfo();
		List<String []> friendsinfo=fillFriendInfo();

		contentSim=compareContent(myinfo, friendsinfo);

		Similarity similarityMeasure = new Similarity(myGraph,friendGraph,  0.0001);
		structureSim=similarityMeasure.getGraphSimilarity();
	}

	public void postMethod(){
		showResPhrase();
		structure.setText("Primary Test = "+round(structureSim,1)+" %");
		content.setText("Secondary Test = "+round(contentSim,1)+" %");
		
		theButton.setVisibility(View.INVISIBLE);
		press.setVisibility(View.INVISIBLE);
		yourscore.setVisibility(View.VISIBLE);
		star.setVisibility(View.VISIBLE);
	}

	public void showResPhrase() {
		double totScore=contentSim+structureSim;
		if (totScore < 20) {
			phrase.setImageResource(R.drawable.notcompat);
			phrase.setVisibility(View.VISIBLE);
		}
		else if (totScore < 60){
			phrase.setImageResource(R.drawable.abitcompat);
			phrase.setVisibility(View.VISIBLE);
		}
		else if (totScore < 100) {
			phrase.setImageResource(R.drawable.moderatelycompat);
			phrase.setVisibility(View.VISIBLE);
		}
		else if (totScore < 150) {
			phrase.setImageResource(R.drawable.goodcompat);
			phrase.setVisibility(View.VISIBLE);
		}
		else {
			phrase.setImageResource(R.drawable.youshould);
			phrase.setVisibility(View.VISIBLE);
		}
		score.setText(""+round((totScore/2),1));
	}

	public double compareContent(List<String []> a, List<String []> b){
		List<String> toComp = new ArrayList<String>(Arrays.asList("birthday","gender","interested_in","relationship_status","inspirational_people","sports","languages","favorite_athletes","hometown","education"));

		double points=0;
		int compared=0;

		for(String[] me:a){
			double seen=0;
			double sameCat=0;
			for (String[] friend : b) {
				if (me[0].equals(friend[0]) && toComp.contains(me[0])) {
					if (me[0].equals("birthday")) {
						SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
						try {
							Date date1 = sdf.parse(me[1]);
							Date date2 = sdf.parse(friend[1]);
							long diff=Math.abs(date1.getTime() - date2.getTime());
							diff=((((diff/1000)/3600)/24)/365);
							if(diff<5){
								points+=1;
							}
							else if (diff<10) {
								points+=0.4;
							}
							else{
								points+=0;
							}
							compared++;
						} catch (ParseException e) {
							System.out.println("Birthday not complete");
						}
						break;
					}
					else if (me[0].equals("gender")){
						if (me[1].equals("male") && friend[1].equals("female") || me[1].equals("female") && friend[1].equals("male")) {
							points+=1;
						}
						else{
							points+=0;
						}
						compared++;
						break;
					}
					else if (me[0].equals("interested_in")){
						if (me[1].equals("male") && friend[1].equals("female") || me[1].equals("female") && friend[1].equals("male")) {
							points+=1;
						}
						else{
							points+=0;
						}
						compared++;
						break;
					}
					else if (me[0].equals("relationship_status")){
						if(me[1].equals("Single") && friend[1].equals("Single")){
							points+=1;
						}
						else {
							points+=0.5;
						}
						compared++;
						break;
					}
					else if (me[0].equals("inspirational_people")){
						if(me[1].equals(friend[1])){
							points+=1;
							break;
						}
						else if (me[2].equals(friend[2])) {
							sameCat++;
						}
						else{
							//
						}
						seen++;
					}
					else if (me[0].equals("sports")){
						if(me[1].equals(friend[1])){
							points+=1;
							break;
						}
						else if (me[2].equals(friend[2])) {
							sameCat++;
						}
						else{
							//
						}
						seen++;
					}
					else if (me[0].equals("languages")){
						if(me[1].equals(friend[1])){
							points+=1;
						}
						compared++;
						break;
					}
					else if (me[0].equals("favorite_athletes")){
						if(me[1].equals(friend[1])){
							points+=1;
							break;
						}
						else if (me[2].equals(friend[2])) {
							sameCat++;
						}
						else{
							//
						}
						seen++;
					}
					else if (me[0].equals("hometown")){
						if(me[1].equals(friend[1])){
							points+=1;
							break;
						}
						else if (me[2].equals(friend[2])) {
							sameCat++;
						}
						else{
							//
						}
						seen++;
					}
					else if (me[0].equals("education")){
						if(me[1].equals(friend[1])){
							points+=1;
							break;
						}
						else if (me[2].equals(friend[2])) {
							sameCat++;
						}
						else{
							//
						}
						seen++;
					}
				}
			}
			if (seen!=0) {
				points+=((sameCat/seen)/2);
				compared++;
			}
		}
		return (round(points/compared,2)*100);
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public List<String []> fillFriendInfo(){
		List<String []> hope0=getCharacteristics(friendsId);

		String name=findName(hope0);
		friendGraph.addNode(userString, name);

		for(String[] x:hope0){
			if (x.length==2) {
				friendGraph.addNode(x[0], x[1]);
				friendGraph.addEdge(userString, name, x[0], x[1]);
			}
			else if (x.length==3) {
				friendGraph.addNode(x[0], x[1]);
				friendGraph.addEdge(userString, name, x[0], x[1]);
				if (!friendGraph.nodeExists(categoryString, x[2])) {
					friendGraph.addNode(categoryString, x[2]);
				}
				friendGraph.addEdge(categoryString, x[2], x[0], x[1]);
			}
			else{
				System.out.println("???");
				System.out.println(x.toString());
			}
		}
		return hope0;
	}

	public List<String[]> fillMyInfo(){
		List<String []> hope0=getCharacteristics("me");

		String name=findName(hope0);
		myGraph.addNode(userString, name);

		for(String[] x:hope0){
			if (x.length==2) {
				myGraph.addNode(x[0], x[1]);
				myGraph.addEdge(userString, name, x[0], x[1]);
			}
			else if (x.length==3) {
				myGraph.addNode(x[0], x[1]);
				myGraph.addEdge(userString, name, x[0], x[1]);
				if (!myGraph.nodeExists(categoryString, x[2])) {
					myGraph.addNode(categoryString, x[2]);
				}
				myGraph.addEdge(categoryString, x[2], x[0], x[1]);
			}
			else{
				System.out.println("???");
				System.out.println(x.toString());
			}
		}
		return hope0;
	}

	public List<String[]> getCharacteristics(String id){

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), id);
		Response response = getCat.executeAndWait();

		return parseMe(response);
	}

	public List<String[]> parseMe(Response user){
		String[] soloCharacteristics={"bio","birthday","locale","link","first_name","timezone","email","verified","name","last_name","gender","id","relationship_status"}; //13
		String[] multiCharacteristics={"inspirational_people","location","sports","languages","favorite_athletes","hometown","education"}; //8
		String[] tableCharacteristics={"interested_in"}; //1
		ArrayList<String[]> informations=new ArrayList<String[]>();
		JSONObject list=user.getGraphObject().getInnerJSONObject();
		for(String key:soloCharacteristics){
			try {
				String value=list.getString(key);
				String[] toAdd={key,value};
				informations.add(toAdd);
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:multiCharacteristics){
			try {
				String valuelist=list.getString(key);
				String result=parseResponse(valuelist);
				String[] resultList=result.split(",");
				for(String x:resultList){
					String[] valueAndCategory=getCategory(x);
					String[] toAdd={key,valueAndCategory[0],valueAndCategory[1]};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:tableCharacteristics){
			try {
				String value=list.getString(key);
				value=value.replace("[", "").replace("]", "").replace("\"", "");
				String[] valueList=value.split(",");
				for(String x:valueList){
					String[] toAdd={key,x};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		return informations;
	}

	public static String parseResponse(String test){
		String result="";
		String[] open=test.split("(\\{)|(,)|(\\})");
		boolean with=false;
		int fromCount=0;
		for(String x:open){
			if(!x.equals("")){
				if(fromCount==0){
					if(x.contains("from")){
						fromCount=2;
					}
					else{
						if(x.contains("]")){
							with=false;
						}
						else if(x.contains("with") || with==true){
							with=true;
						}
						else{
							if(x.contains("\"id\"")){
								String end=x.replace("\"", "").replace("name:", "");
								if(result.equals("")){
									result+=end;
								}
								else{
									result+=","+end;
								}
							}
						}
					}
				}
				else{
					fromCount--;
				}

			}
		}
		result=result.replace("id", "").replace(":", "");
		return result;
	}

	public String[] getCategory(String id){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), id);
		Response response = getCat.executeAndWait();
		String[] result={response.getGraphObject().getProperty("name").toString(),response.getGraphObject().getProperty("category").toString()};
		return result;
	}

	public String findName(List<String[]> info){
		for(String[] tab:info){
			if(tab[0].equals("name")){
				return tab[1];
			}
		}
		return null;
	}
	
	public Bitmap roundCornerImage(Bitmap src, float round) {
		// Source image size
		int width = src.getWidth();
		int height = src.getHeight();
		// create result bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// configure paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// configure rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw Round rectangle to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}
}
