package com.tdewandeleer.loveconnector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.LoginButton;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class HomePage extends Activity implements OnClickListener {

	LoginButton authButton;
	ImageButton compare;
	TextView about;
	ImageButton graph;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homepage);
		authButton = (LoginButton) findViewById(R.id.authButtonHome);
		authButton.setOnClickListener(this);
		compare=(ImageButton) findViewById(R.id.compare);
		compare.setOnClickListener(this);
		about=(TextView) findViewById(R.id.about);
		about.setOnClickListener(this);
		graph=(ImageButton) findViewById(R.id.graph);
		graph.setOnClickListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed()) ) {
			onSessionStateChange(session, session.getState(), null);
		}
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			//Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
		} else if (state.isClosed()) {
			//Toast.makeText(getApplicationContext(), "Disconnected", Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case R.id.compare:
			Intent compare=new Intent(this, ChooseFriend.class);
			startActivity(compare);
			break;
		case R.id.graph:
			myInfo();
			break;
		case R.id.about:
			Toast.makeText(getApplicationContext(),"The Facebook Love Connector\nYves Deville\nUCL 2014-2015", Toast.LENGTH_SHORT).show();
			break;
		case R.id.authButtonHome:
			Session.getActiveSession().closeAndClearTokenInformation();
			finish();
			break;
		}
	}

	public void myInfo(){		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), "me");
		Response response = getCat.executeAndWait();
		
		List<String[]> info=parseMe(response);
		
		String selectedName=findName(info);
		String id=findId(info);
		
		if (selectedName!=null && id!=null) {
			Intent intent=new Intent(this, FriendPage.class);
			intent.putExtra("name", selectedName);
			intent.putExtra("id", id);
			intent.putExtra("myId", id);
			intent.putExtra("me", "Y");
			startActivity(intent);
		}
		else{
			Toast.makeText(getApplicationContext(),"Something went wrong", Toast.LENGTH_SHORT).show();
		}
	}

	public String findName(List<String[]> info){
		for(String[] tab:info){
			if(tab[0].equals("name")){
				return tab[1];
			}
		}
		return null;
	}
	
	public String findId(List<String[]> info){
		for(String[] tab:info){
			if(tab[0].equals("id")){
				return tab[1];
			}
		}
		return null;
	}

	public List<String[]> parseMe(Response user){
		String[] soloCharacteristics={"bio","birthday","locale","link","first_name","timezone","email","verified","name","last_name","gender","id","relationship_status"};
		String[] multiCharacteristics={"inspirational_people","location","sports","languages","favorite_athletes","hometown","education"};
		String[] tableCharacteristics={"interested_in"};
		ArrayList<String[]> informations=new ArrayList<String[]>();
		JSONObject list=user.getGraphObject().getInnerJSONObject();
		for(String key:soloCharacteristics){
			try {
				String value=list.getString(key);
				String[] toAdd={key,value};
				informations.add(toAdd);
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:multiCharacteristics){
			try {
				String valuelist=list.getString(key);
				String result=parseResponse(valuelist);
				String[] resultList=result.split(",");
				for(String x:resultList){
					String[] valueAndCategory=getCategory(x);
					String[] toAdd={key,valueAndCategory[0],valueAndCategory[1]};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		for(String key:tableCharacteristics){
			try {
				String value=list.getString(key);
				value=value.replace("[", "").replace("]", "").replace("\"", "");
				String[] valueList=value.split(",");
				for(String x:valueList){
					String[] toAdd={key,x};
					informations.add(toAdd);
				}
			} catch (JSONException e) {
				System.out.println(key+" NOTFOUND");
			}
		}

		return informations;
	}

	public static String parseResponse(String test){
		String result="";
		String[] open=test.split("(\\{)|(,)|(\\})");
		boolean with=false;
		int fromCount=0;
		for(String x:open){
			if(!x.equals("")){
				if(fromCount==0){
					if(x.contains("from")){
						fromCount=2;
					}
					else{
						if(x.contains("]")){
							with=false;
						}
						else if(x.contains("with") || with==true){
							with=true;
						}
						else{
							if(x.contains("\"id\"")){
								String end=x.replace("\"", "").replace("name:", "");
								if(result.equals("")){
									result+=end;
								}
								else{
									result+=","+end;
								}
							}
						}
					}
				}
				else{
					fromCount--;
				}

			}
		}
		result=result.replace("id", "").replace(":", "");
		return result;
	}

	public String[] getCategory(String id){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), id);
		Response response = getCat.executeAndWait();
		String[] result={response.getGraphObject().getProperty("name").toString(),response.getGraphObject().getProperty("category").toString()};
		return result;
	}
}
