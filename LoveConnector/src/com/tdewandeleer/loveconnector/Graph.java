package com.tdewandeleer.loveconnector;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Graph {
	private Integer[][] graph;
	private int graphSize;
	private List<Integer> nodeList;
	private List<List<Integer>> inDegreeNodeList;
	private List<List<Integer>> outDegreeNodeList;
	private String[][] colorList;

	public Graph(Integer[][] graph,String[][] color) throws Exception {
		try {
			if (graph.length != color.length) {
				throw new Exception();
			}
			this.graph = graph;
			this.graphSize = graph.length;
			this.nodeList = new ArrayList<Integer>();
			this.inDegreeNodeList = new ArrayList<List<Integer>>();
			this.outDegreeNodeList = new ArrayList<List<Integer>>();
			this.colorList = color;
			setDegreeNodeList();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Graph(){
		this.graph = new Integer[0][0];
		this.graphSize = 0;
		this.nodeList = new ArrayList<Integer>();
		this.inDegreeNodeList = new ArrayList<List<Integer>>();
		this.outDegreeNodeList = new ArrayList<List<Integer>>();
		this.colorList = new String[0][2];
	}

	public void setDegreeNodeList() {
		for (int i=0; i<graphSize ; i++){
			inDegreeNodeList.add(new ArrayList<Integer>());
			outDegreeNodeList.add(new ArrayList<Integer>());
		}
		for (int i = 0; i < graphSize; i++) {
			for (int j = 0; j < graphSize; j++) {
				if (graph[i][j] != 0){
					inDegreeNodeList.get(j).add(i);
					outDegreeNodeList.get(i).add(j);
				}
			}
		}
	}

	public Integer[][] getGraph() {
		return graph;
	}

	public int getGraphSize() {
		return graphSize;
	}

	public List<List<Integer>> getInDegreeNodeList() {
		return inDegreeNodeList;
	}

	public List<List<Integer>> getOutDegreeNodeList() {
		return outDegreeNodeList;
	}

	public List<Integer> getNodeList() {
		setNodeList();
		return nodeList;
	}

	public void setNodeList() {
		for(int i=0; i<graphSize;i++){
			nodeList.add(i);
		}
	}

	public void printGraph(){
		System.out.println();
		for(int i=0;i<graphSize;i++){
			System.out.print(getNode(i)[1]+" ("+getNode(i)[0]+") is connected to ");
			if (outDegreeNodeList.get(i).isEmpty()){
				System.out.println("Nothing");
			}
			else{
				Iterator it= outDegreeNodeList.get(i).iterator();
				while (it.hasNext()){
					int index=(Integer) it.next();
					System.out.print(getNode(index)[1]+" ("+getNode(index)[0]+")");
					if(it.hasNext()){
						System.out.print(" , ");
					}
				}
				System.out.println();
			}
		}
		System.out.println();
	}

	public String[] getNode(int i){
		return colorList[i];
	}

	public String getNodeType(int i){
		return colorList[i][0];
	}

	public String getNodeValue(int i){
		return colorList[i][1];
	}

	public void addNode(String type,String value){
		if(nodeExists(type, value)==false){
			//graph
			int previousgraphSize=this.graph.length;
			Integer[][] newGraph = new Integer[previousgraphSize+1][previousgraphSize+1];
			for (int i = 0; i < newGraph.length; i++) {
				for (int j = 0; j < newGraph.length; j++) {
					if((i==previousgraphSize)||(j==previousgraphSize)){
						newGraph[i][j]=0;
					}
					else{
						newGraph[i][j]=this.graph[i][j];
					}

				}
			}
			//graphSize
			this.graphSize=this.graphSize+1;
			//ColorList
			int colorListSize=this.colorList.length;
			String[][] newColor=new String[colorListSize+1][2];
			for (int i = 0; i < colorListSize; i++) {
				for (int j = 0; j < 2; j++) {
					newColor[i][j]=this.colorList[i][j];
				}
			}
			newColor[colorListSize][0]=type;
			newColor[colorListSize][1]=value;
			this.colorList=newColor;
			//IN/OUT
			inDegreeNodeList.add(new ArrayList<Integer>());
			outDegreeNodeList.add(new ArrayList<Integer>());
		}
		else{
			System.err.println("Node "+value+" ("+type+") already exists in the graph");
		}
	}

	public boolean nodeExists(String type,String value){
		boolean exists=false;
		for (int i = 0; i < this.colorList.length; i++) {
			if(this.getNodeType(i).equals(type) && this.getNodeValue(i).equals(value)){
				exists=true;
				break;
			}
		}
		return exists;
	}

	public int getNodeIndex(String type,String value){
		int index=-1;
		for (int i = 0; i < this.colorList.length; i++) {
			if(this.getNodeType(i).equals(type) && this.getNodeValue(i).equals(value)){
				index=i;
			}
		}
		return index;
	}

	public void addEdge(String typeA, String valueA, String typeB, String valueB){
		if (edgeExists(typeA, valueA, typeB, valueB)==false){
			if(nodeExists(typeA, valueA)){
				if(nodeExists(typeB, valueB)){
					//OK
					int indexA= getNodeIndex(typeA, valueA);
					int indexB= getNodeIndex(typeB, valueB);
					inDegreeNodeList.get(indexB).add(indexA);
					outDegreeNodeList.get(indexA).add(indexB);
				}
				else{
					System.err.println("Second node does not exists");
				}
			}
			else{
				System.err.println("First node does not exists");
			}
		}
		else{
			System.err.println("Edge already exists in the graph");
		}
	}
	
	public boolean edgeExists(String typeA, String valueA, String typeB, String valueB){
		int indexA= getNodeIndex(typeA, valueA);
		int indexB= getNodeIndex(typeB, valueB);
		if(inDegreeNodeList.get(indexB).contains(indexA) && outDegreeNodeList.get(indexA).contains(indexB)){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	public List<Integer> getInNeighbour(String type,String value){		
		return inDegreeNodeList.get(getNodeIndex(type, value));
	}
	
	public List<Integer> getInNeighbour(int index){		
		return inDegreeNodeList.get(index);
	}
	
	public List<Integer> getOutNeighbour(String type,String value){
		return outDegreeNodeList.get(getNodeIndex(type, value));
	}
	
	public List<Integer> getOutNeighbour(int index){		
		return outDegreeNodeList.get(index);
	}
	
	public Graph extractSubGraphAround(String type,String value,int range){
		if(this.nodeExists(type, value)){
			ArrayList<Integer> neighbourNodes=new ArrayList<Integer>();
			int index=this.getNodeIndex(type, value);
			neighbourNodes.add(index);
			
			//IN
			for (int i = 0; i < range; i++) {
				ArrayList<Integer> toAdd=new ArrayList<Integer>();
				for(int item: neighbourNodes){
					for(int neighbour: getInNeighbour(item)){
						if(neighbourNodes.contains(neighbour)==false){
							toAdd.add(neighbour);
						}
					}
				}
				for(int add:toAdd){
					neighbourNodes.add(add);
				}
			}
			//OUT
			for (int i = 0; i < range; i++) {
				ArrayList<Integer> toAdd=new ArrayList<Integer>();
				for(int item: neighbourNodes){
					for(int neighbour: getOutNeighbour(item)){
						if(neighbourNodes.contains(neighbour)==false){
							toAdd.add(neighbour);
						}
					}
				}
				for(int add:toAdd){
					neighbourNodes.add(add);
				}
			}
						
			
			Graph toReturn=new Graph();
			for(int node: neighbourNodes){
				toReturn.addNode(getNodeType(node), getNodeValue(node));
			}
			
			for(int node: neighbourNodes){
				for(int neighOut:getOutNeighbour(node)){
					if(toReturn.nodeExists(getNodeType(neighOut), getNodeValue(neighOut))){
						toReturn.addEdge(getNodeType(node), getNodeValue(node), getNodeType(neighOut), getNodeValue(neighOut));
					}
				}
			}
		
			return toReturn;
			
		}
		else{
			System.err.println("First node does not exists");
			return null;
		}
	}
}
