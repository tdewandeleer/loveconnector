package com.tdewandeleer.loveconnector;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ChooseFriend extends Activity implements OnClickListener {

	ArrayList<String[]> friends=new ArrayList<String[]>();
	LinearLayout go;
	TextView nameText;
	LinearLayout lin;
	String[] name;
	String[] id;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friendlist);

		go=(LinearLayout) findViewById(R.id.buttonGo);
		nameText=(TextView) findViewById(R.id.textFriend);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request friendGet=new Request(Session.getActiveSession(), "/me/friends");
		Response response = friendGet.executeAndWait();

		parseFriends(response);
		
		int amountFriends=friends.size();
		name = new String[amountFriends];
		id = new String[amountFriends];

		int i=0;
		for (String[] frienfsInfo : friends) {
			id[i]=frienfsInfo[0];
			name[i]=frienfsInfo[1];
		}
		
		//String[] id={"10201968856781964","none","none","none"};

		go.setOnClickListener(this);
		
		lin=(LinearLayout) findViewById(R.id.linearLayout);
		int j=0;
		for (String[] frienfsInfo : friends) {
			ImageView img=new ImageView(ChooseFriend.this);
			img.setId(j);
			img.setImageResource(R.drawable.circleselector);
			String idfriend=frienfsInfo[0];
			getProfilePicture(idfriend, img);
			lin.addView(img);
			MarginLayoutParams marginParams = new MarginLayoutParams(img.getLayoutParams());
		    marginParams.setMargins(15, 15, 15, 15);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(marginParams);
		    params.width = 400;
		    params.height = 400;
		    img.setLayoutParams(params);
		    img.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	nameText.setText(findName(id[v.getId()]));
	            }
	        });
		    j++;
		}
		

	}

	private void parseFriends(Response response){
		JSONArray names = (JSONArray)response.getGraphObject().getProperty("data");
		for (int i=0; i < names.length(); i++) {
			JSONObject name = names.optJSONObject(i);
			String[] friend={name.optString("id"),name.optString("name")};
			friends.add(friend);
		}
	}

	private String findId(String name){
		for(String[]x:friends){
			if(x[1].equals(name)){
				return x[0];
			}
		}
		return null;
	}

	private String findName(String id){
		for(String[]x:friends){
			if(x[0].equals(id)){
				return x[1];
			}
		}
		return null;
	}

	public String getMyId(){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		Request getCat=new Request(Session.getActiveSession(), "me");
		Response response = getCat.executeAndWait();
		JSONObject list=response.getGraphObject().getInnerJSONObject();
		String id="Error";
		try {
			id=list.getString("id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return id;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.buttonGo:
			String namefriend=nameText.getText().toString();
			if(!namefriend.equals("No One Selected")){
				Intent intent=new Intent(this, FriendPage.class);
				intent.putExtra("name", namefriend);
				intent.putExtra("id", findId(namefriend));
				intent.putExtra("myId", getMyId());
				intent.putExtra("me", "N");
				startActivity(intent);
			}
		}

	}
	
	public void getProfilePicture(String id,ImageView profilePicture){
		if(id!="none"){
			try {
				URL image_value = new URL("https://graph.facebook.com/"+id+ "/picture?type=large");
				Bitmap bmp = null;
				try {
					bmp = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				BitmapDrawable ob = new BitmapDrawable(getResources(), roundCornerImage(bmp, 100));
				profilePicture.setBackgroundDrawable(ob);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		else{
			profilePicture.setBackgroundResource(R.drawable.male);
		}
	}
	public Bitmap roundCornerImage(Bitmap src, float round) {
		// Source image size
		int width = src.getWidth();
		int height = src.getHeight();
		// create result bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// configure paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// configure rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw Round rectangle to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}

}
