package com.tdewandeleer.loveconnector;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] id;

	public CustomListAdapter(Activity context, String[] id) {
		super(context, R.layout.myfriendlist, id);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.id=id;
	}

	public View getView(int position,View view,ViewGroup parent) {
		LayoutInflater inflater=context.getLayoutInflater();
		View rowView=inflater.inflate(R.layout.myfriendlist, null,true);

		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

		getProfilePicture(id[position], imageView);

		return rowView;

	};

	public void getProfilePicture(String id,ImageView profilePicture){
		if(id!=null){
			try {
				URL image_value = new URL("https://graph.facebook.com/"+id+ "/picture?type=large");
				Bitmap bmp = null;
				try {
					bmp = BitmapFactory.decodeStream(image_value.openConnection().getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				profilePicture.setImageBitmap(roundCornerImage(bmp, 100));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		else{
			profilePicture.setImageResource(R.drawable.male);
		}
	}

	public Bitmap roundCornerImage(Bitmap src, float round) {
		// Source image size
		int width = src.getWidth();
		int height = src.getHeight();
		// create result bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// configure paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// configure rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw Round rectangle to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}
}